#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef struct List List;
typedef struct Node Node;

struct Node
{
	Node *next;
	unsigned int data;
};

struct List
{
	Node *head;
};

//print list
void print_list(List *list)
{
	Node *temp = list->head;
	if(temp == NULL)
	{
		printf("The list is empty\n");
	}
	else
	{
		while(temp != NULL)
		{
			printf("%d",temp->data);
			temp = temp->next;
		}
	}
	printf("\n");
}

//create list
List *create_list(void)
{
	List *list=(List*)malloc(sizeof(List));
	list->head = NULL;

	return list;
}

//indexed from 0
int count_elements(List *list)
{
	Node *current = list->head;
	if(current == NULL)
	{
		printf("Empty List\n");
		return 0;
	}

	int count =1;
	while(current->next != NULL)
	{
		current = current->next;
		count++;
	}
	return count;
}
//remove nth el
void remove_nth_element(List *list, int index)
{
	Node *temp1 = list->head;
	int last_index = count_elements(list)-1;

	if(temp1 == NULL)
	{
		printf("Empty list\n");
		return;
	}

	if(index == 0)
	{
		list->head = list->head->next;
		free(temp1);
		return;
	}

	if(index > last_index)
	{
		printf("index %d higher than last index in list %d\n", index, last_index);
		return;
	}

	for(int i=0; i<index-1; i++)
	{
		temp1=temp1->next;
		printf("%d - ", temp1->data);
	}

	Node *temp2 = temp1->next;
	if(index <= last_index)
	{
		temp1->next = temp2->next;
	}

	free(temp2);
}

//append el to list at the end
void append_to_list(List *list, unsigned int elem)
{
	Node *temp = (Node*)malloc(sizeof(Node));
	Node *last = list->head;

	if(temp == NULL)
	{
		printf("Error!\n");
		exit(EXIT_FAILURE);
	}

	temp->data = elem;
	temp->next = NULL;

	if (last == NULL)
	{
		list->head = temp;
	}
	else
	{
		while(last->next != NULL)
			last = last->next;

		last->next = temp;
	}
}

//get n-th el
int get_nth_element(List *list, int index)
{
	Node *current = list->head;

	if(current == NULL)
	{
		printf("Empty list\n");
		return 0;
	}

	int count = 0;
	while(current->next != NULL)
	{
		if(count == index)
			return(current->data);

		count++;
		current = current->next;
	}
	return current->data;
}

int main()
{
	List *list = create_list();

	append_to_list(list,8);
	assert(count_elements(list)==1);

	append_to_list(list,5);
	assert(count_elements(list)==2);

	get_nth_element(list,1);
	print_list(list);
	//assert(get_nth_element(list,1)==8);

	remove_nth_element(list,0);
	assert(count_elements(list)==1);
	print_list(list);
}

