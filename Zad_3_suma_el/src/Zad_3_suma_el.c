#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


int sum(int *tab, int size)
{
	int sum=0;
	int max_id=0;
	int min_id=0;

	for(int i=0; i<size; i++)
	{
		sum+=tab[i];
		if(tab[max_id]<tab[i])
		{
			max_id=i;

		}

		if(tab[min_id]>tab[i])
		{
			min_id=i;
		}
	}
	sum=sum-tab[max_id]-tab[min_id];

	return sum;
}

int main(void)
{
	int tab[]={1,2,3,4,5};
	int size=sizeof(tab)/sizeof(tab[0]);

	int sum_up=sum(tab, size);
	printf("%d\n",sum_up);
	assert(sum_up==9);

	int tab1[]={5,10,3,1,5};
	int size1=sizeof(tab1)/sizeof(tab1[0]);

	int sum_up1=sum(tab1, size1);
	printf("%d\n",sum_up1);
	assert(sum_up==13);



	return 0;
}
