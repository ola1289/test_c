#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define N 20

int anagram(char *str1, char *str2)
{
	int l1 = strlen(str1);
	int l2 = strlen(str2);

	if(l1!=l2)
	{
		printf("Uploaded word are not anagrams\n");
		return 0;
	}

	for(int i=0;i<l1-1;i++)
	{
		for(int j=i+1;j<l1;j++)
		{
			if(str1[i]>str1[j])
			{
				char temp=str1[j];
				str1[i]=str1[j];
				str1[j]=temp;
			}

			if(str2[i]>str2[j])
			{
				char temp=str2[j];
				str2[i]=str2[j];
				str2[j]=temp;
			}
		}

	}

	for(int i=0; i<l1;i++)
	{
		if(str1[i]!=str2[i])
		{
			printf("Uploaded word are not anagrams\n");
			return 0;
		}
		else
		{
			printf("Uploaded word are anagrams\n");
			return 1;
		}
	}

}
int main(void)
{
	char str1[N];
	char str2[N];

	printf("Wpisz pierwsze slowo: \n");
	scanf("%s", str1);

	printf("Wpisz drugie slowo: \n");
	scanf("%s", str2);

	anagram(str1, str2);


	return 0;
}
