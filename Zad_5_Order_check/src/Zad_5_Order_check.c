#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


int orderCheck(int *tab, int size)
{
	int asc = 1;
	int desc = 1;

	for(int i=0; i<size-1; i++)
	{
		if(tab[i]<=tab[i+1])
		{
			asc++;
		}
		else if(tab[i]>=tab[i+1])
		{
			desc++;
		}
		else
			return 0;
	}

	if(asc == size)
	{
		return 1;
	}
	else if(desc == size)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int main()
{
	int tab[]={1,2,3,4,5};
	int size=sizeof(tab)/sizeof(tab[0]);
	int result;

	result = orderCheck(tab,size);
	assert(result==1);

	printf("%d\n", result);

	int tab1[]={5,4,3,2,1};
	int size1=sizeof(tab1)/sizeof(tab1[0]);

	result = orderCheck(tab1,size1);
	assert(result==-1);

	printf("%d\n", result);

	int tab2[]={5,8,2,10,1};
	int size2=sizeof(tab2)/sizeof(tab2[0]);

	result = orderCheck(tab2,size2);
	assert(result==0);

	printf("%d\n", result);

	return 0;
}
